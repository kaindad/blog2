class
CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /posts/1/comments
  # GET /posts/:post_id/comments.json
  def index
    @post = Post.find(params[:post_id])
    @comments = @post.comments.all
    # respond_with(@comments)
  end

  # GET /posts/1/comments/1
  # GET /posts/1/comments/1.json
  def show
    @post = Post.find(params[:post_id])
  end

  # GET /posts/1/comments/new
  def new
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build
    # respond_with(@comment)
  end

  # GET /posts/1/comments/1/edit
  def edit
    @post = Post.find(params[:post_id])
  end

  # POST /posts/1/comments
  # POST /posts/1/comments.json
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:post_id, :body)
    end
end
